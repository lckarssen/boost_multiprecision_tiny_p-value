/**
 * \file z-from-tiny-p.cpp
 * \author L.C. Karssen <l.c.karssen@polyomica.com>
 * \date Time-stamp: <2017-09-29 17:19:04 (L.C. Karssen)>
 *
 * \brief This is a test program to test how to get \f$Z\f$-values
 * from very small \f$\p\f$-values for normally distributed data.
 *
 * It uses the Boost math and multiprecision libraries.
 *
 * Licence: MIT
 */
#include <iostream>
#include <iomanip>

#include <boost/math/distributions/normal.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

using namespace std;


/**
 * \brief Main function
 * \return Integer corresponding to the exit status.
 */
int main(void){
    namespace mp = boost::multiprecision;

    mp::cpp_dec_float_100 z;
    mp::cpp_dec_float_100 p("1e-400");

    // Check to see if we can compute simple things with these high
    // precision numbers:
    cout << "2p = " << 2. * p << endl;

    // Construct a standard normal distribution with mean zero and
    // standard deviation of 1.
    boost::math::normal_distribution<mp::cpp_dec_float_100> ndist(0 ,1.);

    z = quantile(ndist, p);

    // We're not really interested in precision when printing z, so
    // truncate to 5 decimals when printing
    cout << setprecision(5);
    cout << scientific << left << "z = " << z << endl;

    return(0);
}
