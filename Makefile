CXXFLAGS=-I/usr/include/boost
LDLIBS=-lboost_iostreams -lstdc++  -lm

# Needed because otherwise cc is used, in which case -lstdc++
# must be added to -LDLIBS
#CC=g++

PROGRAM=z-from-tiny-p

$(PROGRAM): $(PROGRAM).o

clean:
	$(RM) $(PROGRAM).o $(PROGRAM)
